function calculate() {
    let num1 = parseFloat(prompt("Введіть перше число:"));
    let num2 = parseFloat(prompt("Введіть друге число:"));
    let operation = prompt("Введіть математичну операцію (+, -, *, /):");

    while (isNaN(num1) || isNaN(num2)) {
        num1 = parseFloat(prompt("Введено некоректні значення. Будь ласка, введіть перше число ще раз:"));
        num2 = parseFloat(prompt("Введено некоректні значення. Будь ласка, введіть друге число ще раз:"));
    }

    while (operation !== "+" &&
    operation !== "-" &&
    operation !== "*" &&
    operation !== "/") {
        operation = prompt("Введіть математичну операцію ще раз +, -, *, /");
    }

    let result;
    switch (operation) {
        case "+":
            result = num1 + num2;
            break;
        case "-":
            result = num1 - num2;
            break;
        case "*":
            result = num1 * num2;
            break;
        case "/":
            result = num1 / num2;
            break;
        default:
            console.log("Введена некоректна операція!");
            return;
    }

    console.log("Результат:", result);
}

calculate();